import { Component, Input, OnInit } from '@angular/core';
import { Image } from '../model/Image';
import { PassDataService } from '../services/pass-data.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {


  imageSrc: Image = {name: "", src: ""};


  constructor(private passDataService: PassDataService) {
    passDataService.get().subscribe(image => this.imageSrc = image)
  }

  ngOnInit(): void {
  }

}
