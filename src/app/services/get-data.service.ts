import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Image } from '../model/Image';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  private serverUrl = 'http://localhost:8081';
  imageList: BehaviorSubject<Image[]>

  constructor(private http: HttpClient) {
    this.imageList = new BehaviorSubject<Image[]>([])
  }

  /** GET Images from the server */
  getImages(): Observable<Image[]> {
  return this.http.get<Image[]>(this.serverUrl)
  }

  pushImage(image: Image) {
    return this.http.get<Image>(`${this.serverUrl}/newImage?src=${image.src}&name=${image.name}`)
  }

  deleteImage(image: Image) {
    return this.http.delete<Image>(`${this.serverUrl}/image?src=${image.src}&name=${image.name}`)
  }

  modifyImage(imageOld: Image, imageNew: Image) {
    return this.http.post<Image>(`${this.serverUrl}/set`, {
      imageOld: imageOld,
      imageNew: imageNew
    })

  }

  get(): Observable<Image[]> {
    return this.imageList.asObservable();
  }

  nextImageList(imageList: Image[]) {
    this.imageList.next(imageList)
  }
}
