import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Image } from '../model/Image';
import { GetDataService } from './get-data.service';

@Injectable({
  providedIn: 'root'
})
export class PassDataService {

  image: BehaviorSubject<Image>

  imageList: Image[] = [];

  constructor(private getDataService: GetDataService) {
    this.image = new BehaviorSubject<Image>({name: "", src: ""})
    this.getDataService.getImages().subscribe(
      (data) =>
        {
          this.imageList = data
          this.nextImage(this.imageList[0])
        }
      )

  }

  get(): Observable<Image> {
    return this.image.asObservable();
  }

  nextImage(image: Image) {
    this.image.next(image)
  }
}
