/**
 * Interface for storing data that needs to be shown in the menu
 */
 export interface MenuItem {
  /** Name of the menuitem that should be displayed */
  title: string;
  /** Url to the component */
  url: string;
  /** Icon of the MenuPoint to the component */
  icon: string;
}
