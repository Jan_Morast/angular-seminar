import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Image } from '../model/Image';
import { GetDataService } from '../services/get-data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TableComponent implements OnInit {

  dataSource: Image[] = [];
  displayedColumns = ["image", "name", "source", "edit", "delete"];
  expandedElement: Image | null = null

  addNewImageForm : FormGroup;
  changeImageForm : FormGroup;

  constructor(private getDataService: GetDataService) {
    this.getAllImages()
    this.addNewImageForm = new FormGroup(
      {
        name: new FormControl('', Validators.required),
        src: new FormControl('', Validators.required),
    })
    this.changeImageForm = new FormGroup(
      {
        newName: new FormControl('', Validators.required),
        newSrc: new FormControl('', Validators.required),
    })
  }

  getAllImages(): void {
    this.getDataService.getImages().subscribe(
      (data) => {
        this.dataSource = data
        this.getDataService.nextImageList(this.dataSource)
      })
  }

  ngOnInit(): void {
  }

  onDelete(image : Image) {
    this.getDataService.deleteImage(image).subscribe()
    this.getAllImages()
  }

  onSubmit() {
    let image: Image = {name: this.addNewImageForm.controls['name'].value, src: this.addNewImageForm.controls['src'].value}
    this.getDataService.pushImage(image).subscribe()
    this.getAllImages()
  }

  setExpandedElement(element: Image) {
    this.expandedElement = element
    this.changeImageForm.controls['newName'].setValue(element.name)
    this.changeImageForm.controls['newSrc'].setValue(element.src)
  }

  onChange(oldImage: Image) {
    console.log(oldImage)
    let newImage: Image = {src: this.changeImageForm.controls['newSrc'].value, name: this.changeImageForm.controls['newName'].value}
    console.log(newImage)
    this.getDataService.modifyImage(oldImage, newImage).subscribe()
    this.getAllImages();
  }
}
