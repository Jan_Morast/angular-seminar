import { Component, OnInit } from '@angular/core';
import { Image } from '../model/Image';
import { GetDataService } from '../services/get-data.service';
import { PassDataService } from '../services/pass-data.service';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {

  imageButtons: Image[] = []

  constructor(private passDataService: PassDataService,
    private getDataService: GetDataService) {
      this.getImageList()
      this.getDataService.get().subscribe(
        (data) => {this.getImageList()})
  }

  getImageList() {
    this.getDataService.getImages().subscribe(
      (data) =>
      this.imageButtons = data
    )
  }

  onClick(image: Image) {
    this.passDataService.nextImage(image)
  }

}
