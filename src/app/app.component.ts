import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from './model/MenuItem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: String = 'my-app';

  /** Liste der Menü-Einträge */
  menuList: MenuItem[];

  /**
   * Initializes the list item that are needed in the nav list
   */
  constructor(public router: Router) {
    this.menuList = [
      {
        title: 'Home',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'Galerie',
        url: '/galery',
        icon: 'collections'
      },
      {
        title: 'Tabelle',
        url: '/table',
        icon: 'table_chart'
      },
    ]
  }

}
